function limiter(cb, time) {
	var a = null;
	return function(e) {
		var b = (new Date()).getTime();
		if (!a || b-a > time) {
			a = (new Date()).getTime();
			cb();
		}
	}
};

$('#app').mousemove(limiter(function() {
 	console.log('fire');
}, 1000));