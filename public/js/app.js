define(['backbone', 'backbone.marionette', 'views', 'backbone.localstorage'], function(Backbone, Marionette, views) {
    'use strict';

    var app = new Marionette.Application();

    app.BookSet = Backbone.Collection.extend({
        localStorage: new Backbone.LocalStorage("bookSet"),
    });

    app.bookSet = new app.BookSet();

    app.addRegions({
        form: '#region-form',
        list: '#region-list',
    });

    app.addInitializer(function() {
        this.renderForm();
        this.renderList();
    });

    app.renderForm = function(options) {
        if (typeof options === 'undefined') { options = {}; }

        var model = options.model || new Backbone.Model();
        this.formView = new views.Form({model: model});
        this.form.show(this.formView);
        this.listenTo(this.formView, 'save', this.save, this);
    }

    app.renderList = function() {
        this.bookSet.fetch();
        this.listView = new views.List({collection: this.bookSet});
        this.list.show(this.listView);
        this.listenTo(this.listView, 'childview:remove', function(childView) {
            this.bookSet.remove(childView.model);
            this.renderForm();
        }, this);
        this.listenTo(this.listView, 'childview:edit', function(childView) {
            this.renderForm({model: new Backbone.Model(childView.model.attributes)});
        })
    }

    app.save = function(book) {
        if (!book.id) {
            book.set('id', (new Date()).getTime());
            this.bookSet.create(book);
        } else {
            var oldBook = this.bookSet.get(book.id);
            if (oldBook) { oldBook.set(book.attributes); }
        }
        this.renderForm();
    }

    return app;
});
