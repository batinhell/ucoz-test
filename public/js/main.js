(function() {
    'use strict';

    require.config({
        baseUrl: 'js',
        paths: {
            'backbone': '../vendors/backbone/backbone',
            'backbone.localstorage': '../js/backbone.LocalStorage',
            'backbone.marionette': '../vendors/backbone.marionette/lib/backbone.marionette',
            'jquery': '../vendors/jquery/dist/jquery',
            'underscore': '../vendors/underscore/underscore',
            'ich': '../vendors/icanhaz/ICanHaz'
        },
        shim: {
            'ich': {exports: 'ich'},
            'backbone': {exports: 'Backbone'},
            'backbone.localstorage': {deps: ['backbone', 'underscore']}
        }
    });

    require(['app'], function(app) {
        (window.app = app).start();
    });

})();
