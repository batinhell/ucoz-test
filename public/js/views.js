define(['backbone.marionette', 'jquery', 'underscore', 'ich'], function(Marionette, $, _, ich) {
    'use strict';

    ich.grabTemplates();

    Marionette.TemplateCache.prototype.loadTemplate = function(id) {
        return ich['tpl-' + id];
    }

    Marionette.TemplateCache.prototype.compileTemplate = function(raw) {
        return raw;
    }

    var views = {};

    views.Form = Marionette.ItemView.extend({
        template: 'form',
        events: {
            'submit form': 'onSubmitForm',
            'change input': 'onChangeInput'
        },
        onSubmitForm: function(e) {
            e.preventDefault();
            this.trigger('save', this.model);
        },
        onChangeInput: function(e) {
            var $el = $(e.currentTarget);
            var attr = $el.attr('name');
            var value = $el.val();
            this.model.set(attr, value);
        },
        onRender: function() {
            _.each(_.keys(this.model.attributes), function(k) {
                this.$el.find('input[name="' + k + '"]').val(this.model.get(k));
            }, this);
        }
    });

    views.Item = Marionette.ItemView.extend({
        template: 'item',
        tagName: 'li',
        className: 'list-group-item',
        triggers: {
            'click .ui-remove': 'remove',
            'click .ui-edit': 'edit'
        },
        onRender: function() {
            this.listenTo(this.model, 'change', this.render, this);
        }
    });

    views.List = Marionette.CollectionView.extend({
        childView: views.Item,
        tagName: 'ul',
        className: 'list-group',
    });

    return views;
});
